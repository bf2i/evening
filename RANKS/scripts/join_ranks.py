import pandas as pd
import numpy as np
import functools
import random
from grenadine.Evaluation import evaluation
from grenadine.Inference import inference
list_method = ["|z-score_rows|abs_pearsonr.csv",
              "|z-score_rows|abs_spearmanr.csv",
              "|z-score_rows|AdaBoost_classifier.csv",
              "|z-score_rows|AdaBoost_regression.csv",
              "|z-score_rows|BayesianRidgeScore_regression.csv",
              "|z-score_rows|CLR.csv",
              "|z-score_rows|GB_classifier.csv",
              "|z-score_rows|GENIE3_regression.csv",
               "|z-score_rows|GRNBoost2_regression.csv",
              "|z-score_rows|KendallTau.csv",
              "|z-score_rows|RF_classifier.csv",
              "|z-score_rows|stability_randomizedlasso_regression.csv",
              "|z-score_rows|SVM_classifier.csv",
              "|z-score_rows|SVR_regression.csv",
              "|z-score_rows|TIGRESS_regression.csv",
              "|z-score_rows|XGENIE3_regression.csv",
              "|z-score_rows|XRF_classifier.csv"]
              
              
import functools
score_path_cerevisiae = "/home/baptiste/Documents/Stage/scerevisiae/"
cerevisiae_score_files = [score_path_cerevisiae+"scerevisiae"+i for i in list_method]
cerevisiae_score_files = list(map(functools.partial(pd.read_csv, index_col=0),
                                  cerevisiae_score_files))
cerevisiae_ranks = list(map(functools.partial(evaluation.rank_GRN,  take_abs_score=False),
                                  cerevisiae_score_files))
dic = {list_method[i]:cerevisiae_ranks[i] for i in range(len(list_method))}
ranks, scores = inference.join_rankings_scores_df(**dic)
ranks.to_csv("joined_ranks_cerevisiae.csv")


score_path_aureus = "/home/baptiste/Documents/Stage/saureus/"
aureus_score_files = [score_path_aureus+"saureus"+i for i in list_method]
aureus_score_files = list(map(functools.partial(pd.read_csv, index_col=0),
                                  aureus_score_files))
aureus_ranks = list(map(functools.partial(evaluation.rank_GRN,  take_abs_score=False),
                                  aureus_score_files))
dic = {list_method[i]:aureus_ranks[i] for i in range(len(list_method))}
ranks, scores = inference.join_rankings_scores_df(**dic)
ranks.to_csv("joined_ranks_aureus.csv")

score_path_silico = "/home/baptiste/Documents/Stage/insilico/"
silico_score_files = [score_path_aureus+"silico"+i for i in list_method]
silico_score_files = list(map(functools.partial(pd.read_csv, index_col=0),
                                  silico_score_files))
silico_ranks = list(map(functools.partial(evaluation.rank_GRN,  take_abs_score=False),
                                  silico_score_files))
dic = {list_method[i]:silico_ranks[i] for i in range(len(list_method))}
ranks, scores = inference.join_rankings_scores_df(**dic)
ranks.to_csv("joined_ranks_silico.csv")

score_path_coli = "/home/baptiste/Documents/Stage/ecoli/"
coli_score_files = [score_path_aureus+"ecoli"+i for i in list_method]
coli_score_files = list(map(functools.partial(pd.read_csv, index_col=0),
                                  coli_score_files))
coli_ranks = list(map(functools.partial(evaluation.rank_GRN,  take_abs_score=False),
                                  coli_score_files))
dic = {list_method[i]:coli_ranks[i] for i in range(len(list_method))}
ranks, scores = inference.join_rankings_scores_df(**dic)
ranks.to_csv("joined_ranks_coli.csv")


