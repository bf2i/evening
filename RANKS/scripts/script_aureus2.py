import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import functools
from tqdm.autonotebook import tqdm
from deap import base, creator,tools
import random
import multiprocessing
import time
from grenadine.Evaluation import evaluation
from grenadine.Inference import inference
from dream5.grn import load_grn_saureus
grn_aureus = load_grn_saureus()
init = time.clock_gettime(0)
list_method = ["|z-score_rows|abs_pearsonr.csv",
              "|z-score_rows|abs_spearmanr.csv",
              "|z-score_rows|AdaBoost_classifier.csv",
              "|z-score_rows|AdaBoost_regression.csv",
              "|z-score_rows|BayesianRidgeScore_regression.csv",
              "|z-score_rows|CLR.csv",
              "|z-score_rows|GB_classifier.csv",
              "|z-score_rows|GENIE3_regression.csv",
               "|z-score_rows|GRNBoost2_regression.csv",
              "|z-score_rows|KendallTau.csv",
              "|z-score_rows|RF_classifier.csv",
              "|z-score_rows|stability_randomizedlasso_regression.csv",
              "|z-score_rows|SVM_classifier.csv",
              "|z-score_rows|SVR_regression.csv",
              "|z-score_rows|TIGRESS_regression.csv",
              "|z-score_rows|XGENIE3_regression.csv",
              "|z-score_rows|XRF_classifier.csv"]
              
              
import functools
ranks=pd.read_csv("joined_ranks_aureus.csv")
creator.create("FitnessMin", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

def random_boolean(p): 
  treshold = np.random.uniform(0,1)
  if p>=treshold:
    return 1
  else :
    return 0
  return random.randint(0,1)

IND_SIZE = len(list_method)

def convert(boolean):
    met=[]
    for i in range(len(boolean)):
        if boolean[i]:
          met.append(list_method[i])
    return(met)  

def evaluate(individual):
  all_zero =True
  for i in individual:
    if i:
      all_zero=False
      break
  if all_zero:
    return((0,))
  else :
    met = convert(individual)
    rank_ensemble = ranks[met].mean(axis=1)
    rank_ensemble = rank_ensemble.nsmallest(100000)
    y_true, y_pred, y_pred_binary = evaluation.get_y_targets(grn_aureus,  ranks = rank_ensemble)
    return((evaluation.roc_auc_score(y_true,y_pred),))

toolbox = base.Toolbox()
for prob in list(map(lambda x :x/10,range(5,6))):
    hall_of_fame=[]
    for nrun in range(1):
        toolbox.register("attribute", random_boolean,p=prob)
        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attribute, n=IND_SIZE)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        pool = multiprocessing.Pool()
        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutFlipBit, indpb=1/IND_SIZE)
        toolbox.register("select", tools.selTournament, tournsize=5)
        toolbox.register("evaluate", evaluate)
        toolbox.register("map", pool.map)
        stats = tools.Statistics(key=lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)


        pop = toolbox.population(n=100)
        CXPB, MUTPB, NGEN = 0.5, 10/len(pop), 10
        stat =[]
        # Evaluate the entire population
        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in tqdm(zip(pop, fitnesses)):
          ind.fitness.values = fit

        for g in tqdm(range(NGEN)):
          # Select the next generation individuals
          offspring = toolbox.select(pop, len(pop))
          # Clone the selected individuals
          offspring = list(map(toolbox.clone, offspring))

          # Apply crossover and mutation on the offspring
          for child1, child2 in list(zip(offspring[::2], offspring[1::2])):
            if random.random() < CXPB:
              toolbox.mate(child1, child2)
              del child1.fitness.values
              del child2.fitness.values

          for mutant in offspring:
            if random.random() < MUTPB:
              toolbox.mutate(mutant)
              del mutant.fitness.values
          
          
          # Evaluate the individuals with an invalid fitness
          invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
          fitnesses = map(toolbox.evaluate, invalid_ind)
          for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
          record = stats.compile(pop)
          stat.append(record)
          # The population is entirely replaced by the offspring
          pop[:] = offspring 
        hall_of_fame.append(pop[np.argmax(fitnesses)])       
        avg = list(map(lambda x:x['avg'],stat))
        fit_mins = list(map(lambda x:x['min'],stat))
        fit_max = list(map(lambda x:x['max'],stat))
        pd.DataFrame(stat).to_csv('stats_aureus'+'_'+str(NGEN)+'_'+str(prob)+'_'+str(nrun)+'.csv')
    pd.DataFrame(hall_of_fame,columns=list_method).to_csv('best_ind_aureus'+'_'+str(NGEN)+'_'+str(prob)+'_'+'.csv')
print(time.clock_gettime(0)-init)    
