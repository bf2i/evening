import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import functools
from tqdm.autonotebook import tqdm
from deap import base, creator,tools
import random
import multiprocessing
from grenadine.Evaluation import evaluation

from dream5.grn import load_grn_saureus
from dream5.grn import load_grn_ecoli
from dream5.grn import load_grn_scerevisiae
grn_coli = load_grn_ecoli()

list_method = ["|z-score_rows|abs_pearsonr.csv",
              "|z-score_rows|abs_spearmanr.csv",
              "|z-score_rows|AdaBoost_classifier.csv",
              "|z-score_rows|AdaBoost_regression.csv",
              "|z-score_rows|BayesianRidgeScore_regression.csv",
              "|z-score_rows|CLR.csv",
              "|z-score_rows|GB_classifier.csv",
              "|z-score_rows|GENIE3_regression.csv",
               "|z-score_rows|GRNBoost2_regression.csv",
              "|z-score_rows|KendallTau.csv",
              "|z-score_rows|RF_classifier.csv",
              "|z-score_rows|stability_randomizedlasso_regression.csv",
              "|z-score_rows|SVM_classifier.csv",
              "|z-score_rows|SVR_regression.csv",
              "|z-score_rows|TIGRESS_regression.csv",
              "|z-score_rows|XGENIE3_regression.csv",
              "|z-score_rows|XRF_classifier.csv"]
              
              
import functools
score_path_coli = "/home/data/bsorin/ecoli/"
coli_score_files = [score_path_coli+"ecoli"+i for i in list_method]
coli_score_files = list(map(functools.partial(pd.read_csv, index_col=0),
                                  coli_score_files))
def norm(score):
    x=score.values.flatten()
    std=x[~np.isnan(x)].std()
    mean=x[~np.isnan(x)].mean()
    return((score-mean)/std)
coli_score_normalized=list(map(lambda x:norm(x),coli_score_files))

creator.create("FitnessMin", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

def random_boolean(p): 
  treshold = np.random.uniform(0,1)
  if p>=treshold:
    return 1
  else :
    return 0
  return random.randint(0,1)

IND_SIZE = len(list_method)

def evaluate(individual):
  all_zero =True
  for i in individual:
    if i:
      all_zero=False
  if all_zero:
    return((0,))
  else :
    tmp=[]
    for i in range(len(individual)):
      if individual[i]:
        tmp.append(coli_score_files[i])
    score = sum(tmp)/len(tmp)
    res = evaluation.evaluate_result(scores=score,gold_std_grn=load_grn_ecoli())
    return((res["AUROC"],))

toolbox = base.Toolbox()
for prob in list(map(lambda x :x/10,range(5,7))):
    hall_of_fame=[]
    for nrun in range(10):
        toolbox.register("attribute", random_boolean,p=prob)
        toolbox.register("individual", tools.initRepeat, creator.Individual,
                         toolbox.attribute, n=IND_SIZE)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        pool = multiprocessing.Pool()
        toolbox.register("mate", tools.cxTwoPoint)
        toolbox.register("mutate", tools.mutFlipBit, indpb=2/IND_SIZE)
        toolbox.register("select", tools.selTournament, tournsize=3)
        toolbox.register("evaluate", evaluate)
        toolbox.register("map", pool.map)
        stats = tools.Statistics(key=lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)


        pop = toolbox.population(n=100)
        CXPB, MUTPB, NGEN = 0.5, 1/IND_SIZE, 10
        stat =[]
        # Evaluate the entire population
        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in tqdm(zip(pop, fitnesses)):
          ind.fitness.values = fit

        for g in tqdm(range(NGEN)):
          # Select the next generation individuals
          offspring = toolbox.select(pop, len(pop))
          # Clone the selected individuals
          offspring = list(map(toolbox.clone, offspring))

          # Apply crossover and mutation on the offspring
          for child1, child2 in list(zip(offspring[::2], offspring[1::2])):
            if random.random() < CXPB:
              toolbox.mate(child1, child2)
              del child1.fitness.values
              del child2.fitness.values

          for mutant in offspring:
            if random.random() < MUTPB:
              toolbox.mutate(mutant)
              del mutant.fitness.values
          
          
          # Evaluate the individuals with an invalid fitness
          invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
          fitnesses = map(toolbox.evaluate, invalid_ind)
          for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
          record = stats.compile(pop)
          stat.append(record)
          # The population is entirely replaced by the offspring
          pop[:] = offspring 
        hall_of_fame.append(pop[np.argmax(fitnesses)])       
        avg = list(map(lambda x:x['avg'],stat))
        fit_mins = list(map(lambda x:x['min'],stat))
        fit_max = list(map(lambda x:x['max'],stat))
        pd.DataFrame(stat).to_csv('stats_coli'+'_'+str(NGEN)+'_'+str(prob)+'_'+str(nrun)+'.csv')
    pd.DataFrame(hall_of_fame,columns=list_method).to_csv('best_ind_coli'+'_'+str(NGEN)+'_'+str(prob)+'_'+'.csv')
    
